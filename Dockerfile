FROM worthy98/cent-nodejs
WORKDIR app
COPY . /app
RUN npm i
CMD npm start
EXPOSE 8080
